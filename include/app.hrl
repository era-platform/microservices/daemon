%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021

%% ====================================================================
%% Types
%% ====================================================================

-record(dstate, {
    domain :: binary(),
    site :: binary(),
    self :: pid(),
    ref :: reference(),
    syncref :: reference(),
    start_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer(),
    %
    srvapps = [] :: [map()],
    %
    subscrid :: binary(),
    %
    meta_ref :: reference(),
    meta_timerref :: reference(),
    meta_hc :: non_neg_integer(),
    meta_last_ts :: non_neg_integer()
}).

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(ClassesCN, <<"classes">>).
-define(SrvAppsCN, <<"srvapps">>).

-define(MasterStorageInstance, <<"master">>).
-define(MasterStorageType, <<"postgresql">>).
-define(MasterStorageMode, <<"category">>).

-define(Workdir(Domain), filename:join([?CFG:workdir(),?BU:to_list(Domain)])).
-define(Workdir(Domain,Name), filename:join([?CFG:workdir(),?BU:to_list(Domain),?BU:to_list(Name)])).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(PLATFORMLIB, platformlib).

-define(MsvcDms, <<"dms">>).
-define(MsvcWs, <<"ws">>).

-define(APP, daemon).
-define(MsvcType, <<"daemon">>).

-define(SUPV, daemon_supv).
-define(CFG, daemon_config).
-define(DomainTreeCallback, daemon_domaintree_callback).

-define(DSUPV, daemon_domain_supv).
-define(DSRV, daemon_domain_srv).
-define(Refresher, daemon_domain_refresher).
-define(LeaderSrv, daemon_domain_leader_srv).
-define(PortSrv, daemon_domain_port_srv).

-define(VALIDATOR, daemon_validator_srv).
-define(ValidatorSrvApps, daemon_validator_coll_srvapps).

-define(FixturerCallback, daemon_fixturer_callback).
-define(FixturerLeaderName, 'daemon_fixturer_leader_srv').
-define(FixturerClasses, daemon_fixture_coll_classes).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).
-define(BLfilelib, basiclib_wrapper_filelib).
-define(BLfile, basiclib_wrapper_file).
-define(BLLeaderSrv, basiclib_leader_svc_srv).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_ATTACH, platformlib_dms_crud_attach).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_leader_supv).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {daemon,erl}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
