%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.08.2021
%%% @doc

-module(daemon_domain_port_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start/1,
         stop/1,
         check_alive/1]).
-export([start_link/2]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(pstate, {
    srvapp :: map(),
    port :: undefined | port(),
    os_pid :: undefined | non_neg_integer(),
    regname :: atom(),
    domain :: binary(),
    name :: binary(),
    ref :: reference(),
    last_exit_status :: undefined | integer(),
    last_exit_ts = [] :: [non_neg_integer()]
}).

%% ====================================================================
%% API functions
%% ====================================================================

%%
start(SrvApp) ->
    Domain = maps:get(x_domain,SrvApp),
    RegName = get_regname(SrvApp),
    ChildSpec = {RegName, {?MODULE, start_link, [RegName,SrvApp]}, permanent, 1000, worker, [?MODULE]},
    ?DSUPV:start_child(Domain,ChildSpec).

%%
stop(SrvApp) ->
    RegName = get_regname(SrvApp),
    case whereis(RegName) of
        undefined -> {error,noproc};
        Pid when is_pid(Pid) -> gen_server:call(Pid, 'stop')
    end.

%%
check_alive(SrvApp) ->
    RegName = get_regname(SrvApp),
    case whereis(RegName) of
        undefined -> false;
        Pid when is_pid(Pid) -> true
    end.

%%
start_link(RegName,SrvApp) ->
    gen_server:start_link({local, RegName}, ?MODULE, SrvApp#{x_regname => RegName}, []).

%%
get_regname(SrvApp) ->
    Domain = maps:get(x_domain,SrvApp),
    Name = maps:get(name,SrvApp),
    ?BU:to_atom_new(?BU:strbin("daemon_port_dom:~ts;n:~ts",[Domain,Name])).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(SrvApp) ->
    Ref = make_ref(),
    Domain = maps:get(x_domain,SrvApp),
    Name = maps:get(<<"name">>,SrvApp),
    State1 = #pstate{regname=maps:get(x_regname,SrvApp),
                     srvapp=SrvApp,
                     domain=Domain,
                     name=Name,
                     ref=Ref},
    gen_server:cast(self(), {'init',Ref}),
    ?LOG('$info', "'~ts' '~ts' port srv inited", [Domain,Name]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call('stop', From, #pstate{}=State) ->
    do_stop(State),
    gen_server:reply(From,ok),
    {noreply,stopping};

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'init',Ref}, #pstate{ref=Ref}=State) ->
    do_init(State);

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({'reinit',Ref}, #pstate{ref=Ref}=State) ->
    do_init(State);

handle_info({Port,{data,_Data}}, #pstate{port=Port,os_pid=OsPid}=State) ->
    ?LOG('$trace',"~p data: ~120tp",[OsPid,_Data]),
    {noreply,State};

handle_info({Port,{exit_status,ExitStatus}}, #pstate{port=Port,os_pid=OsPid}=State) ->
    ?LOG('$trace',"~p exit_status: ~120tp",[OsPid,ExitStatus]),
    {noreply,State#pstate{last_exit_status=ExitStatus}};

handle_info({Port,eof}, #pstate{port=Port,os_pid=OsPid}=State) ->
    ?LOG('$info',"~p eof",[OsPid]),
    on_eof(State);

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------------------------
%% Init and re-init
%% ---------------------------------------------------
do_init(#pstate{srvapp=SrvApp}=State) ->
    CmdLine = maps:get(x_cmdline,SrvApp),
    Domain = maps:get(x_domain,SrvApp),
    Name = maps:get(<<"name">>,SrvApp),
    WorkDir = maps:get(x_workdir,SrvApp),
    ?LOG('$trace',"Opening port for '~ts' '~ts' ('~ts')...",[Domain, Name, CmdLine]),
    case catch erlang:open_port({spawn,?BU:to_list(CmdLine)},[binary,exit_status,stream,eof,stderr_to_stdout,{cd,WorkDir}]) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Open port crashed: ~120tp",[Reason]),
            timer:sleep(1000),
            {error,Reason};
        {error,_}=Err ->
            ?LOG('$error',"Open port result: ~120tp",[Err]),
            timer:sleep(1000),
            Err;
        Port when is_port(Port) ->
            {os_pid,OsPid} = erlang:port_info(Port,os_pid),
            PidFilePath = maps:get('x_pidfile_path',SrvApp),
            file:write_file(PidFilePath,?BU:to_binary(OsPid)),
            ?LOG('$info',"'~ts' '~ts' Process started: ~120tp",[Domain,Name,OsPid]),
            ?BLmonitor:start_monitor(self(), [fun() -> kill_process(OsPid,PidFilePath,true) end, fun() -> close_port(Port) end]),
            update_entity_started(State),
            State1 = State#pstate{port=Port, os_pid=OsPid},
            {noreply,State1}
    end.

%% ---------------------------------------------------
%% When external stop (takeover or srvapp deleted)
%% ---------------------------------------------------
do_stop(#pstate{regname=RegName,domain=Domain,port=Port,os_pid=OsPid,srvapp=SrvApp}) ->
    PidFilePath = maps:get('x_pidfile_path',SrvApp),
    kill_process(OsPid,PidFilePath,false),
    close_port(Port),
    ?LOG('$trace',"Process killed: ~120tp",[OsPid]),
    ?BLmonitor:stop_monitor(self()),
    spawn(fun() ->
              ?DSUPV:terminate_child(Domain,RegName),
              ?DSUPV:delete_child(Domain,RegName)
          end),
    ok.

%% @private
kill_process(OsPid,PidFilePath,Async) ->
    ?LOG('$trace',"Killing process ~p...", [OsPid]),
    F = fun() ->
            case erlang:open_port({spawn,?BU:str("kill ~p",[OsPid])}, [binary,stream,stderr_to_stdout]) of
                {error,_}=Err ->
                    ?LOG('$error',"Kill process error (os_pid=~p): ~120tp",[OsPid,Err]), ok;
                Port when is_port(Port) ->
                    case read_pidfile(PidFilePath) of
                        {error,enoent} ->
                            ?LOG('$error',"Pidfile not found (os_pid=~p)",[OsPid]), ok;
                        {error,invalid_data} ->
                            ?LOG('$error',"Pidfile contains invalid data (os_pid=~p)",[OsPid]),
                            case file:delete(PidFilePath) of
                                ok -> ok;
                                {error,_}=Err ->
                                    ?LOG('$error',"Pidfile delete error (os_pid=~p): ~120tp",[OsPid,Err]), ok
                            end;
                        {ok,OsPid} ->
                            case file:delete(PidFilePath) of
                                ok -> ok;
                                {error,_}=Err ->
                                    ?LOG('$error',"Pidfile delete error (os_pid=~p): ~120tp",[OsPid,Err]), ok
                            end;
                        {ok,OsPid2} ->
                            ?LOG('$error',"Pidfile contains wrong data. Expected os_pid=~p, Found os_pid=~p",[OsPid,OsPid2]), ok
                    end end end,
    case Async of
        true -> spawn(F);
        false -> F()
    end.

%% @private
read_pidfile(PidFilePath) ->
    case file:read_file(PidFilePath) of
        {error,enoent} -> {error,enoent};
        {ok,Data} ->
            case ?BU:to_int(Data,undefined) of
                undefined -> {error,invalid_data};
                OsPid -> {ok,OsPid}
            end end.

%% @private
close_port(Port) ->
    catch erlang:port_close(Port).

%% ---------------------------------------------------
%% When os process found down
%% ---------------------------------------------------
on_eof(#pstate{port=Port,srvapp=SrvApp,last_exit_ts=LETS,last_exit_status=LES}=State) ->
    NowTS = ?BU:timestamp(),
    LETS1 = [NowTS|LETS],
    LETS2 = case length(LETS1) of
               N when N > 10 -> lists:droplast(LETS1);
               _ -> LETS1
           end,
    close_port(Port),
    file:delete(maps:get('x_pidfile_path',SrvApp)),
    State1 = State#pstate{port=undefined,
                          os_pid=undefined,
                          last_exit_ts=LETS2},
    case maps:get(<<"restart_mode">>,maps:get(<<"opts">>,SrvApp),<<"permanent">>) of
        <<"temporary">> -> to_disable(State1);
        <<"transient">> when LES==0 -> to_disable(State1);
        <<"transient">> -> to_restart(State1);
        <<"permanent">> -> to_restart(State1)
    end.

%% @private
to_restart(#pstate{last_exit_ts=LETS,ref=Ref}=State) ->
    NowTS = ?BU:timestamp(),
    case LETS of
        [_,_,_,_,TS5|_] when NowTS - TS5 < 10000 ->
            ?LOG('$trace',"Last 5 starts terminated in 10 sec. Pausing for 5 sec..."),
            erlang:send_after(5000,self(),{'reinit',Ref}),
            {noreply,State};
        [_,_,_,TS4|_] when NowTS - TS4 < 10000 ->
            ?LOG('$trace',"Last 4 starts terminated in 10 sec. Pausing for 5 sec..."),
            erlang:send_after(3000,self(),{'reinit',Ref}),
            {noreply,State};
        [_,_,TS3|_] when NowTS - TS3 < 10000 ->
            ?LOG('$trace',"Last 3 starts terminated in 10 sec. Pausing for 1 sec..."),
            erlang:send_after(1000,self(),{'reinit',Ref}),
            {noreply,State};
        _ ->
            do_init(State)
    end.

%% @private
to_disable(#pstate{srvapp=SrvApp}=State) ->
    ?BLmonitor:stop_monitor(self()),
    check_update_entity(fun() -> update_entity_disabled(State) end, State),
    % terminate port server
    spawn(fun() ->
                ?PortSrv:stop(SrvApp),
                ?LeaderSrv:stop(SrvApp)
          end),
    {noreply,State}.

%% ---------------------------------------------------
%% Update entity routines
%% ---------------------------------------------------

%% Update entity, disable and set exited ext info
update_entity_disabled(#pstate{domain=Domain,srvapp=SrvApp,os_pid=OsPid,last_exit_status=ExitStatus}) ->
    CurSite = ?PCFG:get_current_site(),
    CurNode = ?BU:to_binary(node()),
    update_entity(Domain,SrvApp,#{<<"enabled">> => false,
                                  <<"ext">> => #{CurNode => #{<<"status">> => <<"exited">>,
                                                              <<"exit_status">> => ExitStatus,
                                                              <<"exit_time">> => ?BU:strdatetime3339(?BU:utcdatetime_ms()),
                                                              <<"node">> => CurNode,
                                                              <<"site">> => CurSite,
                                                              <<"os_pid">> => OsPid}}}).

%% Update entity, clear exit ext info
update_entity_started(#pstate{domain=Domain,srvapp=SrvApp}) ->
    Id = maps:get(<<"id">>,SrvApp),
    Map0 = #{object => {'entity',Id,[]}},
    FunReply = fun(Reply) -> Reply end,
    ClassMeta = ?FixturerClasses:srvapps(Domain),
    Update = case catch ?DMS_CRUD:crud({Domain,'read',ClassMeta,?SrvAppsCN,Map0,[]}, FunReply) of
                 {'EXIT',_} -> false;
                 {error,_} -> false;
                 {ok,ItemX} -> maps:is_key(<<"exit_status">>,maps:get(<<"ext">>,ItemX))
             end,
    case Update of
        false -> ok;
        true ->
            CurNode = ?BU:to_binary(node()),
            update_entity(Domain,SrvApp,#{<<"ext">> => #{CurNode => null}})
    end.

%% Check if only one running server
check_update_entity(Fun,#pstate{domain=_Domain,srvapp=SrvApp}) ->
    CurSite = ?PCFG:get_current_site(),
    Opts = maps:get(<<"opts">>,SrvApp),
    OneSite = case maps:get(<<"site_mode">>,Opts,<<"any">>) of
                  <<"any">> -> true;
                  <<"all">> -> true; % TODO: true if cursite is first lexical name of all sites where domain is serviced
                  <<"selected">> ->
                      SelectedSites = ordsets:from_list(maps:get(<<"selected_sites">>,Opts,[])),
                      DomainSites = [CurSite], % TODO: get all sites where domain is serviced
                      length(ordsets:intersection(SelectedSites,DomainSites)) == 1
              end,
    OneServer = case maps:get(<<"mode">>,Opts,<<"active-passive">>) of
                    <<"active-active">> -> ?PCFG:get_nodes_by_msvc(?MsvcType) == 1;
                    <<"active-passive">> -> true
                end,
    case OneSite andalso OneServer of
        false -> false;
        true -> Fun()
    end.

%% Call update entity
update_entity(Domain,SrvApp,UpdateContent) when is_map(UpdateContent) ->
    Id = maps:get(<<"id">>,SrvApp),
    Map0 = #{object => {'entity',Id,[]},
             content => UpdateContent},
    FunReply = fun(Reply) -> Reply end,
    ClassMeta = ?FixturerClasses:srvapps(Domain),
    case catch ?DMS_CRUD:crud({Domain,'update',ClassMeta,?SrvAppsCN,Map0,[]}, FunReply) of
        {'EXIT',_} -> false;
        {error,_} -> false;
        {ok,ItemX} -> maps:is_key(<<"exit_status">>,maps:get(<<"ext">>,ItemX))
    end.