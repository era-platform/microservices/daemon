%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.08.2021
%%% @doc

-module(daemon_domain_leader_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/1,
         stop/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% starts leader-election server
start(SrvApp) ->
    Domain = maps:get('x_domain',SrvApp),
    StartOpts = get_startopts(SrvApp),
    RegName = get_regname(SrvApp),
    ChildSpec = {RegName, {?BLLeaderSrv, start_link, [StartOpts]}, permanent, 1000, worker, [?BLLeaderSrv]},
    ?DSUPV:start_child(Domain,ChildSpec).

%% stops leader-election server
stop(SrvApp) ->
    Domain = maps:get('x_domain',SrvApp),
    RegName = get_regname(SrvApp),
    case ?DSUPV:get_childspec(Domain,RegName) of
        {ok,_} ->
            catch ?DSUPV:terminate_child(Domain,RegName),
            catch ?DSUPV:delete_child(Domain,RegName);
        {error,_} -> {error,noproc}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% atomic reg/link name of leader-election server
get_regname(SrvApp) ->
    Domain = maps:get(x_domain,SrvApp),
    Name = maps:get(name,SrvApp),
    ?BU:to_atom_new(?BU:strbin("~ts_leader_dom:~ts;n:~ts",[?MsvcType,Domain,Name])).

%% @private
%% prepare start opts for leader-election server
get_startopts(SrvApp) ->
    RegName = get_regname(SrvApp),
    Domain = maps:get('x_domain',SrvApp),
    Name = maps:get(<<"name">>,SrvApp),
    Opts = maps:get(<<"opts">>,SrvApp),
    HeartbeatInterval = maps:get(<<"heartbeat_interval">>,Opts,5000),
    FunStart = fun() ->
                    Res = ?PortSrv:start(SrvApp),
                    ?LOG('$trace',"'~ts' Started sync port srv '~ts': ~120tp", [Domain,Name,Res]),
                    Res
               end,
    FunStop = fun() ->
                    case ?PortSrv:check_alive(SrvApp) of
                        true ->
                            Res = (catch ?PortSrv:stop(SrvApp)),
                            ?LOG('$trace',"'~ts' Stopped sync port srv '~ts': ~120tp", [Domain,Name,Res]);
                        false ->
                            ?LOG('$trace',"'~ts' Sync port srv '~ts' already terminated", [Domain,Name])
                    end
              end,
    StartOpts = [{regname,RegName},
                 {fun_nodes,fun() -> get_nodes(SrvApp) end},
                 {fun_start,FunStart},
                 {fun_stop,FunStop},
                 {ping_timeout,HeartbeatInterval+1000},
                 {heartbeat_timeout,HeartbeatInterval}, % less than ping
                 {autosort,false},
                 {waittimes,5},
                 {monitor,true},
                 {dbl_restart_master,true},
                 {heartbeat,true},
                 {takeover,true},
                 {fun_out, fun(Fmt,Args) -> ?LOG('$info',Fmt,Args) end}],
    StartOpts.

%% @private
%% return nodes of current msvc on current site
get_nodes(SrvApp) ->
    Name = maps:get(<<"name">>,SrvApp),
    Domain = maps:get('x_domain',SrvApp),
    %
    F = fun() ->
            % SrvApp nodes
            Nodes = ?PCFG:get_nodes_by_msvc(?MsvcType),
            % sort nodes by static pseudo-random (depends on domain,node,name)
            Nodes1 = lists:map(fun({_,Node}) -> Node end,
                               lists:sort(
                                   lists:map(fun(Node) -> {erlang:phash2({Domain,Node,Name}), Node} end, Nodes))),
            ?LOG('$info',"'~ts' '~ts' nodes for leader-election: ~n\t~120tp",[Domain,Name,Nodes1]),
            Nodes1
        end,
    %
    CfgHash = ?PCFG:get_cfg_hash(),
    Key = {?MsvcType,nodes,Domain,Name},
    case ?BLstore:find_u(Key) of
        {_,{CfgHash,Nodes}} -> Nodes;
        _ ->
            Nodes = F(),
            ?BLstore:store_u(Key,{CfgHash,Nodes}),
            Nodes
    end.