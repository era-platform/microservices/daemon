%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.08.2021
%%% @doc routines of srvapps changes appliance

-module(daemon_domain_refresher).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([refresh/2,
         check_notified_item_changed/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

refresh(NowTS,#dstate{domain=Domain,srvapps=OldItems,site=Site,meta_hc=HC}=State) ->
    case ?DMS_CACHE:read_nocache(Domain,?SrvAppsCN,#{},HC) of
        {error,Reason}=Err ->
            ?LOG('$error',"'~ts' refresh error: ~120tp",[Domain,Reason]),
            Err;
        {ok,not_changed} -> {ok,State#dstate{meta_last_ts=NowTS}};
        {ok,NewItems,HC1} ->
            NewItems1 = prepare_items(Domain,Site,NewItems),
            {ok,Created,Deleted,_Unchanged,ModifiedEx} = ?BU:juxtapose_ex(OldItems,NewItems1,fun(Item) -> maps:get(<<"id">>,Item) end),
            ?LOG('$info',"'~ts' refresh: ~120tp",[Domain,{length(Created),length(Deleted),length(_Unchanged),length(ModifiedEx)}]),
            ok = stop_srvapps(Deleted,true),
            ok = stop_srvapps([Old || {Old,_} <- ModifiedEx],false), % name could change, so stop by old
            {ok,NotStarted1} = start_srvapps(Created),
            {ok,NotStarted2} = start_srvapps([New || {_,New} <- ModifiedEx]),
            case NotStarted1 ++ NotStarted2 of
                [] -> {ok,State#dstate{meta_hc=HC1,srvapps=NewItems1,meta_last_ts=NowTS}};
                NotStarted -> {ok,State#dstate{meta_hc=-1,srvapps=NewItems1--NotStarted,meta_last_ts=NowTS}}
            end
    end.

%% -------------
check_notified_item_changed(_,undefined,_) -> false;
%%
check_notified_item_changed('update',ItemN,#dstate{domain=Domain,site=CurSite,srvapps=SrvApps}) ->
    Id = maps:get(<<"id">>,ItemN),
    %ItemN1 = maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_atom_new(K) => V} end, #{}, ItemN),
    ItemN1 = ItemN,
    InState = lists:foldl(fun(ItemX,false) -> case maps:get(id,ItemX) of Id -> ItemX; _ -> false end;
                             (_,Acc) -> Acc
                          end, false, SrvApps),
    New = case prepare_items(Domain,CurSite,[ItemN1]) of
              [] -> false;
              [ItemN2] -> ItemN2
          end,
    InState /= New;
%%
check_notified_item_changed('create',ItemN,#dstate{domain=Domain,site=CurSite}) ->
    %ItemN1 = maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_atom_new(K) => V} end, #{}, ItemN),
    ItemN1 = ItemN,
    length(prepare_items(Domain,CurSite,[ItemN1])) > 0;
%%
check_notified_item_changed('delete',ItemN,#dstate{srvapps=SrvApps}) ->
    Id = maps:get(<<"id">>,ItemN),
    InState = lists:foldl(fun(ItemX,false) -> case maps:get(<<"id">>,ItemX) of Id -> ItemX; _ -> false end;
                             (_,Acc) -> Acc
                          end, false, SrvApps),
    InState /= false.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% Prepare items
%% ------------------------------
prepare_items(Domain,CurSite,Items) ->
    Items1 = [maps:without([<<"ext">>],Item) || Item <- Items],
    Items2 = lists:filter(fun(Item) -> maps:size(maps:get(<<"attachment_info">>,maps:get(<<"opts">>,Item),#{})) > 0
                                       andalso maps:get(<<"enabled">>,Item)
                          end, Items1),
    Items3 = lists:filtermap(fun(Item) ->
                                    Opts = maps:get(<<"opts">>,Item),
                                    Mode = maps:get(<<"mode">>,Opts,<<"active-passive">>),
                                    SiteMode = maps:get(<<"site_mode">>,Opts,<<"all">>),
                                    Sites = maps:get(<<"selected_sites">>,Opts,[]),
                                    InSite = case Mode of
                                                 <<"active-active">> -> true;
                                                 <<"active-passive">> -> sync
                                             end,
                                    ExSite = case SiteMode of
                                                 <<"all">> -> true;
                                                 <<"any">> -> true; % TODO: true if cursite is first lexical name of all sites where domain is serviced
                                                 <<"selected">> -> lists:member(CurSite,Sites)
                                             end,
                                    case {InSite,ExSite} of
                                        {_,false} -> false;
                                        {false,_} -> false;
                                        {true,true} -> {true,Item#{x_mode => 'active'}};
                                        {sync,true} -> {true,Item#{x_mode => 'sync'}}
                                    end end, Items2),
    CurNode = node(),
    Items4 = lists:map(fun(Item) ->
                            Name = maps:get(<<"name">>,Item),
                            SrcDir = ?Workdir(Domain,Name),
                            Item#{x_domain => Domain,
                                  x_cmdline => build_cmd(Domain, CurSite, Item),
                                  x_sync_nodes => build_nodes(Domain, CurSite, CurNode, Item),
                                  x_workdir => SrcDir,
                                  x_srcdir => SrcDir,
                                  x_pidfile_path => ?Workdir(Domain,?BU:str("~ts.pid",[Name]))}
                       end, Items3),
    % -------
    % active -to run
    % sync - to choose leader
    % absent to stop server, stop leader, kill process
    % -------
    % make console commands (with parameters).
    % make sync-nodes list for all (only sync could have more than 1. It should use sync-strategy also, because 2nd node should attach as slave with guarantee).
    % make hash for items, containing nodes also.
    % make sync-strategy server for all (active - by
    Items4.

%% --------------------------------------
%% @private
build_cmd(Domain,CurSite,Item) ->
    CmdLine = maps:get(<<"cmdline">>,Item),
    Name = maps:get(<<"name">>,Item),
    Cmd = CmdLine,
    Cmd1 = binary:replace(Cmd,<<"%DOMAIN%">>,Domain,[global]),
    Cmd2 = case string:find(Cmd1,<<"%WEBSERVERS%">>) of
               nomatch -> Cmd1;
               _ ->
                   WsUrls = ?BU:strbin("[~ts]",[?BU:join_binary([<<"\"",Url/binary,"\"">> || Url <- get_ws_urls(Domain,CurSite)],<<",">>)]),
                   binary:replace(Cmd1,<<"%WEBSERVERS%">>,WsUrls,[global])
           end,
    Cmd3 = case string:find(Cmd2,<<"%LOGPATH%">>) of
               nomatch -> Cmd2;
               _ ->
                   {ok,CWD} = file:get_cwd(),
                   LogPath = ?BU:to_binary(filename:join(CWD,?BLlog:get_log_dir(Name))),
                   binary:replace(Cmd2,<<"%LOGPATH%">>,LogPath,[global])
           end,
    Cmd3.

%% --------
%% @private
get_ws_urls(_Domain,_CurSite) ->
    lists:filtermap(fun({WsNode,WsOpts}) ->
                            HttpPort = ?BU:get_by_key('http_port',WsOpts,undefined),
                            HttpsPort = ?BU:get_by_key('https_port',WsOpts,undefined),
                            Addr = ?BU:node_addr(WsNode),
                            case {HttpPort,HttpsPort} of
                                {undefined,undefined} -> false;
                                {undefined,_} ->
                                    % TODO: get domain's https_host
                                    case {error,not_implemented} of
                                        {error,_} -> false;
                                        {ok,[],_} -> false;
                                        {ok,[Item],_} -> {true,?BU:strbin("https://~ts:~p",[maps:get(value,Item),HttpsPort])}
                                    end;
                                {_,_} -> {true,?BU:strbin("http://~ts:~p",[Addr,HttpPort])}
                            end
                    end, ?PCFG:get_msvc_opts(?MsvcWs)).

%% --------
%% @private
build_nodes(Domain, CurSite, CurNode, Item) ->
    Nodes = case maps:get(x_mode,Item) of
                'active' -> [CurNode];
                'sync' ->
                    lists:filtermap(fun({Site,_,_,_}) when Site/=CurSite -> false;
                                       ({_,Node,_,_}) -> {true,Node}
                                    end, ?PCFG:get_nodes_by_msvc(?MsvcType))
            end,
    Name = maps:get(<<"name">>,Item),
    % sort nodes by static pseudo-random (depends on domain,node,name)
    lists:map(fun({_,Node}) -> Node end,
              lists:sort(
                  lists:map(fun(Node) -> {erlang:phash2({Domain,Node,Name}), Node} end, Nodes))).

%% --------------------------------------
%% Stopping srvapp
%% --------------------------------------
stop_srvapps(Items,DeleteArtifacts) ->
    lists:foreach(fun(Item) -> ok = stop_srvapp(Item,DeleteArtifacts) end, Items).

%% @private
stop_srvapp(Item,DeleteArtifacts) ->
    Domain = maps:get('x_domain',Item),
    Name = maps:get(<<"name">>,Item),
    ?LOG('$trace',"'~ts' Stopping srvapp '~ts'...", [Domain,Name]),
    catch ?PortSrv:stop(Item),
    catch ?LeaderSrv:stop(Item),
    delete_artifacts(Item,DeleteArtifacts),
    ok.

%% @private
delete_artifacts(_,false) -> ok;
delete_artifacts(Item,_) ->
    Domain = maps:get('x_domain',Item),
    Name = maps:get(<<"name">>,Item),
    DomainDir = ?Workdir(Domain),
    SrcDir = filename:join(DomainDir,Name),
    AInfoPath = filename:join(DomainDir,?BU:str("~ts.archive-info.json",[Name])),
    ?BU:directory_delete(SrcDir),
    file:delete(AInfoPath),
    ok.

%% --------------------------------------
%% Starting srvapp
%% --------------------------------------
start_srvapps(Items) ->
    lists:foldl(fun(Item,{ok,Acc}) ->
                        case start_srvapp(Item) of
                            false -> {ok,[Item|Acc]};
                            {error,_} -> {ok,[Item|Acc]};
                            {ok,_} -> {ok,Acc}
                        end end, {ok,[]}, Items).

%% @private
start_srvapp(Item) ->
    stop_srvapp(Item,false), % last iteration could remain active srvapp and return with error
    %
    Name = maps:get(<<"name">>,Item),
    Domain = maps:get('x_domain',Item),
    Mode = maps:get('x_mode',Item),
    case extract(Item) of
        false -> false;
        {error,_}=Err ->
            ?LOG('$error',"'~ts' Error starting srvapp '~ts': ~120tp", [Domain,Name,Err]),
            Err;
        {ok,_AInfo} when Mode=='active' ->
            ?LOG('$trace',"'~ts' Starting active port srv '~ts'...", [Domain,Name]),
            Res = ?PortSrv:start(Item),
            ?LOG('$trace',"'~ts' Start active port srv '~ts': ~120tp", [Domain,Name,Res]),
            Res;
        {ok,_AInfo} when Mode=='sync' ->
            ?LOG('$trace',"'~ts' Starting leader-election srv '~ts'...", [Domain,Name]),
            Res = ?LeaderSrv:start(Item),
            ?LOG('$trace',"'~ts' Start leader-election srv '~ts': ~120tp", [Domain,Name,Res]),
            Res
    end.

%% --------------------------------------
%% Archive attachment routines
%% --------------------------------------
extract(Item) ->
    Id = maps:get(<<"id">>,Item),
    Name = maps:get(<<"name">>,Item),
    Domain = maps:get(x_domain,Item),
    ?LOG('$trace',"Extracting archive ('~ts')...", [Name]),
    %
    DomainDir = ?Workdir(Domain),
    DestDir = filename:join(DomainDir,Name),
    AInfoPath = filename:join(DomainDir,?BU:str("~ts.archive-info.json",[Name])),
    AInfo = case file:read_file(AInfoPath) of
                {ok,AInfoData} ->
                    case catch jsx:decode(AInfoData,[return_maps]) of % archive_info.json has same file info
                        ArchiveInfo when is_map(ArchiveInfo) -> maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_atom_new(K) => V} end, #{}, ArchiveInfo);
                        _ -> undefined
                    end end,
    case ?DMS_ATTACH:download(Domain,Item,Id,<<"attachment">>,"archive.zip",AInfo) of
        {error,_} -> {error,archive_not_found};
        {ok,ArchivePath} ->
            case ?BLfilelib:is_regular(ArchivePath) of % attachment file exists
                false -> {error,archive_not_found};
                true ->
                    case ?BU:file_info_hasha(ArchivePath) of
                        {ok,AInfo} ->
                            ?LOG('$trace',"Archive found already extracted ('~ts')", [Name]),
                            {ok, AInfo};
                        {ok,ArchiveInfo1} when AInfo==undefined ->
                            do_extract(Item,DestDir,ArchivePath,ArchiveInfo1,AInfoPath);
                        {ok,ArchiveInfo1} when AInfo==undefined ->
                            ?LOG('$trace',"Archive file differs to attachment_info. Wait for sync ('~ts') ~n\t prev_ainfo: ~120tp~n\t new_ainfo: ~120tp", [Name,AInfo,ArchiveInfo1]),
                            do_extract(Item,DestDir,ArchivePath,ArchiveInfo1,AInfoPath);
                       {error,Reason1} -> {error,{hash_error,Reason1}}
                    end end end.

%% @private
do_extract(Item,DestDir,ArchivePath,ArchiveInfo,AInfoPath) ->
    Name = maps:get(<<"name">>,Item),
    ?BU:directory_delete(DestDir),
    ?BLfilelib:ensure_dir(DestDir),
    case ?BU:cmd_exec(?BU:strbin("unzip -qq -o -d ~ts ~ts 2>&1", [DestDir, ArchivePath])) of
        {0,_} ->
            case ?BU:file_info_hasha(ArchivePath) of
                {ok,ArchiveInfo} ->
                    file:write_file(AInfoPath,jsx:encode(ArchiveInfo)),
                    ?LOG('$trace',"Archive successfully extracted ('~ts')", [Name]),
                    {ok, ArchiveInfo};
                {ok,_} ->
                    {error,{archive_modified,<<"Archive was modified while unpacking">>}};
                {error,Reason2} -> {error,{hash_error,Reason2}}
            end;
        {_,Out} -> {error,{unzip_error,Out}}
    end.