%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc General facade of domain's server apps daemon service.
%%%        Starts console apps
%%%        Subscribes on changes of DMS's srvapps collection.
%%%      Opts:
%%%        regname, domain

-module(daemon_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(MetaRefreshOkTimeout, 60000 + ?BU:random(5000)). % TODO ICP: use default subscription on dc class 'mservices'
-define(MetaRefreshRetryTimeout, 20000 + ?BU:random(5000)).
-define(MetaRefreshErrTimeout, 10000 + ?BU:random(5000)).
-define(MetaRefreshCrashTimeout, 10000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain] = ?BU:extract_required_props(['domain'], Opts),
    % ----
    Ref = make_ref(),
    gen_server:cast(self(),{init,Ref}),
    % ----
    State = #dstate{domain = Domain,
                    site = ?PCFG:get_current_site(),
                    self = Self=self(),
                    ref = Ref,
                    syncref = SyncRef=make_ref(),
                    start_ts = ?BU:timestamp(),
                    subscrid = SubscrId=?BU:strbin("sid_DAEMON:~ts;~ts",[node(),Domain]),
                    meta_ref = Ref,
                    meta_timerref = erlang:send_after(0, self(), {timer_meta,Ref})},
    % ----
    SubscrOpts = #{<<"ttl_first">> => 20,
                  fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?SrvAppsCN,self(),SubscrId,SubscrOpts),
    % ----
    ?LOG('$info', "'~ts' srv inited", [Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$force', "'~ts' srv state: ~n\t~120tp", [Domain, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({init,Ref}, #dstate{ref=Ref}=State) ->
    State1 = do_init(State),
    {noreply, State1};

%% --------------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #dstate{syncref=SyncRef,meta_timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   NowTS when LoadedTS/=undefined, NowTS - LoadedTS < 5000 -> LoadedTS + 5000 - NowTS;
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#dstate{meta_ref=Ref1,
                                  meta_timerref = erlang:send_after(Timeout, self(), {timer_meta,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% TODO ICP: make async
handle_info({timer_meta,Ref}, #dstate{meta_ref=Ref}=State) ->
    {Timeout,State2} = do_refresh(State),
    Ref1 = make_ref(),
    State3 = State2#dstate{meta_ref=Ref1,
                           meta_timerref = erlang:send_after(Timeout, self(), {timer_meta,Ref1})},
    {noreply, State3};

%% --------------
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId,domain=Domain,meta_timerref=TimerRef,meta_last_ts=LastTS}=State) ->
    ?LOG('$trace',"'~ts' srv got notify: ~n\t~160tp", [Domain,EventInfo]),
    EventData = ?BU:get_by_key(<<"data">>,EventInfo,#{}),
    Refresh = case {maps:get(<<"operation">>,EventData,undefined), maps:get(<<"entity">>,EventData,undefined)} of
                  {undefined,_} -> false;
                  {'update'=Op,Entity} -> ?Refresher:check_notified_item_changed(Op,Entity,State);
                  {'create'=Op,Entity} -> ?Refresher:check_notified_item_changed(Op,Entity,State);
                  {'delete'=Op,Entity} -> ?Refresher:check_notified_item_changed(Op,Entity,State);
                  _ -> true
              end,
    case Refresh of
        false -> {noreply, State};
        true ->
            ?BU:cancel_timer(TimerRef),
            Timeout = case ?BU:timestamp() - LastTS of
                          T when T<1000 -> 1000-T;
                          _ -> 100
                      end,
            Ref1 = make_ref(),
            State1 = State#dstate{meta_ref=Ref1,
                                  meta_timerref = erlang:send_after(Timeout, self(), {timer_meta,Ref1})},
            {noreply, State1}
    end;

%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #dstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------
do_init(State) ->
    kill_zombies_by_pidfiles(State),
    State.

%% @private
kill_zombies_by_pidfiles(#dstate{domain=Domain}=State) ->
    Dir = ?Workdir(Domain),
    os:cmd(?BU:str("cd ~ts && find . -name '*.pid' | xargs cat | xargs kill ",[Dir])),
    os:cmd(?BU:str("cd ~ts && find . -name '*.pid' | xargs rm ",[Dir])),
    State.

%% -----------------------------------
do_refresh(#dstate{domain=Domain}=State) ->
    NowTS = ?BU:timestamp(),
    try ?Refresher:refresh(NowTS,State) of
        {ok,#dstate{meta_hc=-1}=State1} -> {?MetaRefreshRetryTimeout,State1};
        {ok,State1} -> {?MetaRefreshOkTimeout,State1};
        {error,_} -> {?MetaRefreshErrTimeout,State}
    catch C:R:ST ->
        ?LOG('$crash',"'~ts' srv state: ~n\t~160tp", [Domain, {C,R,ST}]),
        {?MetaRefreshCrashTimeout,State}
    end.