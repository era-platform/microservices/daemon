%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(daemon_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([srvapps/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "srvapps"
%% return fixtured entity of controlled class
%% -------------------------------------
srvapps(_Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"1f17731c-017b-391f-a004-7cd30a921f58">>),
        <<"classname">> => ?SrvAppsCN,
        <<"name">> => <<"Server applications">>,
        <<"description">> => <<"General platform collection. Server applications.">>,
        <<"storage_mode">> => <<"category">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator", [?MsvcType]),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"lookup_properties">> => [<<"name">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"name">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"cmdline">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"enabled">>,
                <<"data_type">> => <<"boolean">>,
                <<"required">> => false,
                <<"default">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"attachment">>,
                <<"data_type">> => <<"attachment">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"opts">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"ext">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================