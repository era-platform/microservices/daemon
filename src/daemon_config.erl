%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.08.2021
%%% @doc Configuration functions

-module(daemon_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    workdir/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% return log destination for loggerlib, ex. {app,prefix}
log_destination(Default) ->
    ?BU:get_env(?APP,'log_destination',Default).

%% return root work dir for srvapps
workdir() ->
    case ?BU:get_env(?APP,'dir',undefined) of
        undefined ->
            {ok,CWD} = file:get_cwd(),
            filename:join([CWD,"srvapps"]);
        Dir when is_list(Dir) -> Dir
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================